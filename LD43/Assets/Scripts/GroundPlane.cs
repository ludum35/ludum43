using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GroundPlane : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick( PointerEventData eventData )
    {
        if( eventData.button == PointerEventData.InputButton.Right )
        {
            Congregation congregation = SelectionManager.Instance.SelectedCongregation;
            Location t = SelectionManager.Instance.SelectedLocation;

            if( congregation != null )
            {
                if( congregation.m_Occupying != null )
                {
                    congregation.Leave();
                }
                congregation.SetDestination( eventData.pointerCurrentRaycast.worldPosition );
            }
            else if( t != null && t.occupants != null )
            {
                Congregation con = t.occupants;
                SelectionManager.Instance.Select( con );
                con.Leave();
                con.SetDestination( eventData.pointerCurrentRaycast.worldPosition );
            }
            if( eventData.button == PointerEventData.InputButton.Left )
            {
                SelectionManager.Instance.DeselectAll();
            }
        }
    }
}

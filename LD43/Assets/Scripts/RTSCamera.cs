using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTSCamera : MonoBehaviour
{
    public enum Axis
    {
        X = 0,
        Y = 1,
        Z = 2
    }


    public BoxCollider BoundsCollider;

    public float scrollSpeed;
    public float zoomSpeed;

    public float scrollDampening;
    public float zoomDampening;

    public Vector3 CameraMovement = Vector3.zero;

    public Axis VerticalAxis = Axis.Y;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxisRaw( "Horizontal" ) * scrollSpeed;
        float v = Input.GetAxisRaw( "Vertical" ) * scrollSpeed;
        float scroll = Input.GetAxisRaw( "Mouse ScrollWheel" ) * -100f * zoomSpeed;

        float minZoom = 8.0f;
        float maxZoom = 40.0f;

        float newOrthoSize = Mathf.Clamp( Camera.main.orthographicSize + scroll, minZoom, maxZoom);

        float t = ( newOrthoSize - minZoom ) / ( maxZoom - minZoom );
        
        Camera.main.orthographicSize = newOrthoSize;

        if ( VerticalAxis == Axis.Y )
        {
            CameraMovement += new Vector3( h, 0, v ) * Time.deltaTime;
        }
        //keep default case z axis as vertical
        else
        {
            CameraMovement += new Vector3( h, v, 0 ) * Time.deltaTime;
        }


        Vector3 ContainedPos = transform.position + CameraMovement * Mathf.Lerp( 1.0f, 5.0f, t );

        for ( int i = 0; i < 3; i++ )
        {
            ContainedPos[i] = LimitAxis( ContainedPos[i], (Axis)i );
        }

        CameraMovement.x = CameraMovement.x * scrollDampening;
        CameraMovement.y = CameraMovement.y * scrollDampening;
        CameraMovement.z = CameraMovement.z * zoomDampening;


        transform.position = ContainedPos;
    }



    public float LimitAxis( float var, Axis axis )
    {
        Bounds b = BoundsCollider.bounds;

        Vector3 Min = BoundsCollider.bounds.center - BoundsCollider.bounds.extents;
        Vector3 Max = BoundsCollider.bounds.center + BoundsCollider.bounds.extents;

        return Mathf.Min( Max[(int)axis],
                Mathf.Max( Min[(int)axis], var ) );
    }
}

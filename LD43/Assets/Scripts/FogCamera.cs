using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogCamera : MonoBehaviour
{
    public static FogCamera Instance { get; private set; }
    public Vector3 offset;
    private Texture2D m_Fog;

    public float cameraSize;
    public float PlaneSize;

    private void Start()
    {
        Instance = this;

        RenderTexture fogRT = GetComponent<Camera>().targetTexture;
        m_Fog = new Texture2D( fogRT.width, fogRT.height );
    }

    private void OnPostRender()
    {
        RenderTexture fogRT = GetComponent<Camera>().activeTexture;
        RenderTexture lastRT = RenderTexture.active;
        RenderTexture.active = fogRT;


        m_Fog.ReadPixels( new Rect( 0, 0, fogRT.width, fogRT.height ), 0, 0, false );

        RenderTexture.active = lastRT;

    }

    public float GetValue( Vector3 worldPoint )
    {
        Vector3 uvPos = ( new Vector3( cameraSize * 2f, 0.0f, cameraSize ) + (offset + worldPoint) ).Div( new Vector3( PlaneSize * 2f, 1.0f, PlaneSize ) );
        Vector2 pixelPos = new Vector2( uvPos.x, uvPos.z ) * new Vector2( m_Fog.width, m_Fog.height );

        Color c = m_Fog.GetPixel( (int)pixelPos.x, (int)pixelPos.y );


        return c.r;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAnim : MonoBehaviour
{

    public float maxMovement = 1f;
    public Transform moveTransform;
    public Vector3 normalPos;
    public Vector3 lastPos;
    public Vector3 lastTarget;
    Coroutine upDownRoutine;
    public float speed;
    
    bool isRunning = false;
    // Use this for initialization
    void Start()
    {
        if( moveTransform != null )
        {
            normalPos = moveTransform.localPosition;
        }
    }

    private Vector3 getTarget( Vector3 lastTarget )
    {
        Vector3 newTarget = Vector3.zero;
        if( lastTarget.z > ( maxMovement / 2f ) )
        {
            newTarget = new Vector3( 0f, 0f, Random.Range( -maxMovement / 4f, 0f ) );
        }
        else
        {
            newTarget = new Vector3( 0f, 0f, Random.Range( maxMovement / 2f, maxMovement ) );
        }
        this.lastTarget = newTarget;
        return newTarget;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if( ( lastPos - transform.position ).sqrMagnitude > 0.000001f )
        {
            lastPos = transform.position;

            if( upDownRoutine == null )
            {
                Debug.Log( "start" );
                upDownRoutine = StartCoroutine( moveUpDown( getTarget( lastTarget ) ) );
            }
            else if( !isRunning && upDownRoutine != null )
            {
                Debug.Log( "e" );
                StopCoroutine( upDownRoutine );
                upDownRoutine = null;
            }
        }
        else
        {
            if( upDownRoutine == null )
            {
                return;
            }
            Debug.Log( "et" );
            StopCoroutine( upDownRoutine );
            upDownRoutine = null;
        }
    }

    public IEnumerator moveUpDown( Vector3 localTarget )
    {
        isRunning = true;
        Vector3 moveVector = moveTransform.localPosition;
        while( ( localTarget - moveTransform.localPosition ).magnitude > 0.05f )
        {
            moveVector = localTarget - moveTransform.localPosition;
            moveTransform.localPosition += moveVector.normalized * speed * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        isRunning = false;
        yield return null;
    }
}

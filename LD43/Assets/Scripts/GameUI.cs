using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DefaultExecutionOrder( -100 )]
public class GameUI : MonoBehaviour
{
    public static GameUI Instance { get; private set; }

    public Text Devotion;
    public Text ChannelProgress;

    public GameObject VictoryPanel;
    public GameObject DefeatPanel;

    public UIActionPanel UnitActionPanel;

    public enum DisplayPanel
    {
        None,
        Congregation,
        Stronghold
    }

    private DisplayPanel m_CurrentPanel;

    private void Start()
    {
        Instance = this;
    }

    private void Update()
    {
        UpdateUnitActionPanel();
    }

    private bool ShouldUpdate( DisplayPanel newPanel )
    {
        return m_CurrentPanel != newPanel;
    }

    private void UpdateUnitActionPanel()
    {
        Congregation selected = SelectionManager.Instance.SelectedCongregation;

        if( selected == null )
        {
            if( SelectionManager.Instance.SelectedLocation != null )
            {
                selected = SelectionManager.Instance.SelectedLocation.occupants;
            }
        }

        Stronghold stronghold = null;
        if( SelectionManager.Instance.SelectedLocation != null )
            stronghold = SelectionManager.Instance.SelectedLocation.GetComponent<Stronghold>();

        if( stronghold != null )
        {
            if( ShouldUpdate( DisplayPanel.Stronghold ) || stronghold.GetIfRequireUIUpdate() )
            {
                UnitActionPanel.Init( stronghold );
                UnitActionPanel.gameObject.SetActive( true );
                m_CurrentPanel = DisplayPanel.Stronghold;
            }
        }
        else
        {
            if( selected == null )
            {
                UnitActionPanel.gameObject.SetActive( false );
                m_CurrentPanel = DisplayPanel.None;
            }
            else
            {
                if( selected.Location != null )
                    stronghold = selected.Location.GetComponent<Stronghold>();

                if( stronghold != null && ( ShouldUpdate( DisplayPanel.Stronghold ) || stronghold.GetIfRequireUIUpdate() ) )
                {
                    UnitActionPanel.Init( stronghold );
                    m_CurrentPanel = DisplayPanel.Stronghold;
                }

                if( stronghold == null && ( ShouldUpdate( DisplayPanel.Congregation ) || selected.GetIfRequireUIUpdate() ) )
                {
                    UnitActionPanel.Init( selected );
                    m_CurrentPanel = DisplayPanel.Congregation;
                }

                UnitActionPanel.gameObject.SetActive( true );
            }
        }
    }
}

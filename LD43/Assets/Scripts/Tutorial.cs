﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour
{
    public GameObject MoveCamera;
    public GameObject Zoom;
    public GameObject Select;
    public GameObject Move;
    public GameObject Enter;
    public GameObject Inquisition;
    public GameObject Recruit;
    public GameObject Sacrifice;
    public GameObject Leave;
    public GameObject Corrupt;
    public GameObject End;

    int a;

    float timer;

    void Start()
    {
        Zoom.SetActive( false );
        Select.SetActive( false );
        Move.SetActive( false );
        Enter.SetActive( false );
        Recruit.SetActive( false );
        Sacrifice.SetActive( false );
        Inquisition.SetActive( false );
        Leave.SetActive( false );
        Corrupt.SetActive( false );
        End.SetActive( false );
    }

    void Update()
    {

        GameState.Instance.AddDevotion( 1000 );

        if( MoveCamera.activeInHierarchy )
        {
            a |= Input.GetKeyDown( KeyCode.A ) ? 1 : 0;
            a |= Input.GetKeyDown( KeyCode.D ) ? 2 : 0;
            a |= Input.GetKeyDown( KeyCode.W ) ? 4 : 0;
            a |= Input.GetKeyDown( KeyCode.S ) ? 8 : 0;

            if( a == 15 )
            {
                MoveCamera.SetActive( false );
                Zoom.SetActive( true );
            }
        }
        else if( Zoom.activeInHierarchy )
        {
            float scroll = Input.GetAxis( "Mouse ScrollWheel" );

            if( scroll != 0 )
            {
                Zoom.SetActive( false );
                Select.SetActive( true );
            }
        }
        else if( Select.activeInHierarchy )
        {
            if( SelectionManager.Instance.SelectedCongregation != null )
            {
                Select.SetActive( false );
                Move.SetActive( true );
            }
        }
        else if( Move.activeInHierarchy )
        {
            if( SelectionManager.Instance.SelectedCongregation != null )
            {
                if( Input.GetMouseButtonDown( 1 ) )
                {
                    Move.SetActive( false );
                    Enter.SetActive( true );
                }
            }
        }
        else if( Enter.activeInHierarchy )
        {
            if( FindObjectOfType<Congregation>().m_Occupying != null )
            {
                Enter.SetActive( false );
                Inquisition.SetActive( true );
            }
        }
        else if( Inquisition.activeInHierarchy )
        {
            if( Input.GetMouseButton( 0 ) )
            {
                Inquisition.SetActive( false );
                Recruit.SetActive( true );
            }
        }
        else if( Recruit.activeInHierarchy )
        {
            SelectionManager.Instance.Select( FindObjectOfType<Location>() );
            if( Input.GetMouseButtonDown( 0 ) )
            {
                Recruit.SetActive( false );
                Sacrifice.SetActive( true );
            }
        }
        else if( Sacrifice.activeInHierarchy )
        {
            if( Input.GetMouseButtonDown( 0 ) )
            {
                Sacrifice.SetActive( false );
                Leave.SetActive( true );
            }
        }
        else if( Leave.activeInHierarchy )
        {
            if( FindObjectOfType<Congregation>().m_Occupying == null )
            {
                Leave.SetActive( false );
                Corrupt.SetActive( true );
            }
        }
        else if( Corrupt.activeInHierarchy )
        {
            if( GameState.Instance.CastingSpell != SpellType.None )
            {
                Corrupt.SetActive( false );
                timer = 5.0f;
            }
        }
        else
        {            
            if( GameState.Instance.CastingSpell == SpellType.None )
            {
                End.SetActive( true );
                timer -= Time.deltaTime;
                if( timer <= 0.0f )
                {
                    SceneManager.LoadScene( "GameScene" );
                }
            }
        }
    }
}

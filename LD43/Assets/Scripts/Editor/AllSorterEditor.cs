﻿using UnityEditor;
using UnityEngine;


[CustomEditor( typeof( AllSorter ) )]
public class AllSorterEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        AllSorter myScript = (AllSorter)target;
        if( GUILayout.Button( "S O R T" ) )
        {
            SpriteRenderer[] renderer = myScript.GetComponentsInChildren<SpriteRenderer>();
            foreach( SpriteRenderer r in renderer )
            {
                if( r.name.Contains( "road" ) || r.name.Contains( "floor" ) )
                    r.sortingOrder = ( -(int)( r.transform.position.z * 10.0f ) ) - 40;
                else if( r.name.Contains( "river" ) )
                    r.sortingOrder = ( -(int)( r.transform.position.z * 10.0f ) ) - 100;
                else if( r.name.Contains( "fan" ) )
                    r.sortingOrder = ( -(int)( r.transform.position.z * 10.0f ) ) + 10;
                else
                    r.sortingOrder = -(int)( r.transform.position.z * 10.0f );

            }
        }
    }
}
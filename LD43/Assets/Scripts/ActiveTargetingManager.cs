using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder( -200 )]
public class ActiveTargetingManager : MonoBehaviour
{
    public static ActiveTargetingManager Instance { get; private set; }

    public List<ITargetingUser> users;

    public ITargetingUser defaultUser;

    public ITargetingUser activeUser;

    public GameObject ActiveUser;

    public void Start()
    {
        users = new List<ITargetingUser>();
        Instance = this;
    }

    public void Update()
    {
        if( activeUser != null )
        {
            ActiveUser = activeUser.gameObject;
        }
        else
        {
            ActiveUser = null;
        }
    }

    public void Register( ITargetingUser user, bool isDefaultUser )
    {
        if( !users.Contains( user ) )
        {
            users.Add( user );
        }

        if( isDefaultUser )
        {
            defaultUser = user;
            SetAsActive( defaultUser );
        }

        user.requestActive = RequestActive;
    }

    public void RequestActive( ITargetingUser user )
    {
        SetAsActive( user );
    }

    public void GiveControl( ITargetingUser user )
    {
        if( activeUser == user )
        {
            activeUser.setInActive();
        }
        else
        {
            return;
        }

        SetAsActive( defaultUser );
    }

    public void SetAsActive(ITargetingUser user)
    {
        for( int i = 0; i < users.Count; i++ )
        {
            if( users[i] != user )
            {
                users[i].setInActive();
            }
        }
        activeUser = user;
        user.setActive();
    }
}

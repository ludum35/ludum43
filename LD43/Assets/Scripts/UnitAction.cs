using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActionID
{
    None,
    EnterTown,
    LeaveTown,
    Recruit,
    Sacrifice,
    MakeStronghold,
    CreateCongregation,
    DoomPortal
}

public class UnitAction : ScriptableObject
{
    public string Name;

    [TextArea]
    public string ToolTip;
    public Sprite Icon;

    public ActionID ID;
    public GameObject FX;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIGameEvent : MonoBehaviour
{
    public TextMeshProUGUI textBox;
    public Vector3 pos;
    public Transform moveTransform;
    public Vector3 movementPerFrame = new Vector3( 0, 0, .2f );

    public void Start()
    {

    }

    public void Update()
    {
        pos += movementPerFrame * Time.deltaTime;
        
        if (moveTransform == null)
        {
            transform.position += pos;
        }
        else
        {
            moveTransform.position += pos;
        }
    }
}

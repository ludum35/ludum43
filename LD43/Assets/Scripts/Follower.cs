﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent( typeof( NavMeshAgent ) )]
public class Follower : MonoBehaviour
{
    public List<Sprite> Sprites;

    public GameObject Following;

    public float IdleRadius = 3.0f;

    private float m_WaitTime;
    private Vector3 m_Target;
    private NavMeshAgent m_Agent;
    private Vector3 m_FixedOffset;

    public void Start()
    {
        Sprite sprite = Sprites[Random.Range( 0, Sprites.Count )];
        GetComponentInChildren<SpriteRenderer>().sprite = sprite;

        m_WaitTime = Random.Range( 2.0f, 6.0f );

        m_Agent = GetComponent<NavMeshAgent>();

        m_Agent.acceleration = Random.Range( 25.0f, 100.0f );

        m_FixedOffset = new Vector3( Random.Range( -IdleRadius, IdleRadius ), 0.0f, Random.Range( -IdleRadius, IdleRadius ) );
    }

    public void FixedUpdate()
    {
        if( Following == null )
        {
            Destroy( gameObject );
            return;
        }

        m_Agent.speed = Following.GetComponent<NavMeshAgent>().speed;

        if( m_WaitTime > 0.0f )
        {
            if( Vector3.Distance( Following.transform.position, transform.position ) > IdleRadius )
            {
                m_Agent.SetDestination( Following.transform.position + m_FixedOffset );
            }
            else
            {
                m_WaitTime -= Time.deltaTime;

                if( m_WaitTime <= 0.0f )
                {
                    m_Target = new Vector3( Random.Range( -IdleRadius, IdleRadius ), 0.0f, Random.Range( -IdleRadius, IdleRadius ) );

                    m_Agent.SetDestination( Following.transform.position + m_Target );
                }
            }
        }

        if( m_WaitTime <= 0.0f )
        {
            float d = Vector3.Distance( transform.position, Following.transform.position + m_Target );
            if( d < 0.25f || d > IdleRadius * 2.0f )
            {
                m_WaitTime = Random.Range( 2.0f, 6.0f );
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogOfWarHider : MonoBehaviour
{
    public List<GameObject> Hide;
    public bool IsVisible { get; private set; }

    private void FixedUpdate()
    {
        IsVisible = GameState.Instance.GetIfInfluence( transform.position );

        foreach( GameObject go in Hide )
        {
            go.SetActive( IsVisible );
        }
    }
}

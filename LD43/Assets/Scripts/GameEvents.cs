using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameEvents : MonoBehaviour
{
    public enum Type
    {
        Positive,
        Negative
    }

    public static GameEvents Instance { get; private set; }

    public Transform WorldUIParent;

    public UIGameEvent PositiveEvent;
    public UIGameEvent NegativeEvent;

    private void Start()
    {
        Instance = this;
    }

    public GameObject CreateWorldUI( Type type, string text, Vector3 pos )
    {
        GameObject prefab = null;
        if ( type == Type.Positive )
        {
            prefab = PositiveEvent.gameObject;
        }
        if (type == Type.Negative)
        {
            prefab = NegativeEvent.gameObject;
        }

        GameObject uiEvent = Instantiate( prefab, WorldUIParent );


        uiEvent.GetComponent<UIGameEvent>().textBox.text = text;
        uiEvent.GetComponent<UIGameEvent>().transform.position = pos;
        return uiEvent;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoverTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [TextArea]
    public string Text;

    public bool ScreenSpace = true;

    public Transform Anchor;

    private int id;

    public void Start()
    {
        id = Tooltip.NextID;
    }

    public void OnPointerEnter( PointerEventData eventData )
    {
        Vector3 position = Anchor.transform.position;
        if( !ScreenSpace )
            position = Camera.main.WorldToScreenPoint( position );

        Tooltip.Instance.ShowTooltip( id, position, Text );
    }

    public void OnPointerExit( PointerEventData eventData )
    {
        Tooltip.Instance.EndTooltip( id );
    }
}

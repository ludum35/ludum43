using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Party : MonoBehaviour
{
    public GameObject Leader;
    public GameObject FollowerPrefab;
    public int MinSize;
    public int MaxSize;
    public int MinFollowers;
    public int MaxFollowers;
    public GameObject DeathFX;
    public GameObject SpawnFX;

    public Func<int> getNumFollowers;

    private List<GameObject> m_Followers = new List<GameObject>();

    public void SetNumFollowers( int N )
    {
        float t = Mathf.Clamp01( ( (float)N - (float)MinSize ) / ( (float)MaxSize - (float)MinSize ) );

        SetNormalizedFollowers( Mathf.RoundToInt( Mathf.Lerp( MinFollowers, MaxFollowers, t ) ) );
    }

    private void SetNormalizedFollowers( int followers )
    {
        while( m_Followers.Count < followers )
        {
            GameObject instance = Instantiate( FollowerPrefab );
            
            instance.transform.position = (transform.position) + new Vector3( Random.Range( -1.0f, 1.0f ), 0.0f, Random.Range( -1.0f, 1.0f ) );
            instance.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;



            m_Followers.Add( instance );

            instance.GetComponent<Follower>().Following = gameObject;

            if( SpawnFX != null )
            {
                GameObject fxInstance = Instantiate( SpawnFX, transform );
                fxInstance.transform.position = instance.transform.position;
            }
        }

        while( m_Followers.Count > followers )
        {
            GameObject instance = m_Followers[m_Followers.Count - 1];
            m_Followers.Remove( instance );

            if( DeathFX != null )
            {
                GameObject fxInstance = Instantiate( DeathFX );
                fxInstance.transform.position = instance.transform.position;
            }

            Destroy( instance );
        }
    }
}

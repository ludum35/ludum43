﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortingLayerMeshThing : MonoBehaviour
{
    public int TargetLayer;

    void Start()
    {
        GetComponent<MeshRenderer>().sortingOrder = TargetLayer;

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Location ) )]
public class Stronghold : MonoBehaviour
{
    public List<UnitActionCallBackPair> actions = new List<UnitActionCallBackPair>();
    
    public GameObject CongregationPrefab;

    private bool m_HadOccupantsLastFrame = true;
    public bool m_Channeling { get; private set; }
    private float m_ChannelTimer;

    

    void Start()
    {
        m_Channeling = false;
        actions.AddRange( new UnitActionCallBackPair[]
        {
            new UnitActionCallBackPair( ActionID.CreateCongregation, () => CreateCongregation() ),
            new UnitActionCallBackPair( ActionID.DoomPortal, () => ChannelDoomPortal() ),
            new UnitActionCallBackPair( ActionID.LeaveTown, () => CongregationLeave() )
        } );

    }

    void FixedUpdate()
    {
        if( m_Channeling )
        {
            Congregation congregation = GetComponent<Location>().occupants;
            float delta = 5.0f / (float)congregation.NumFollowers;

            m_ChannelTimer += Time.deltaTime;
            while( m_ChannelTimer >= delta )
            {
                m_ChannelTimer -= delta;

                if( GameState.Instance.CanAffordDevotion( 1 ) )
                {
                    GameState.Instance.ChannelProgress++;
                    GameState.Instance.ConsumeDevotion( 1 );
                }
            }
        }
    }

    public void CreateCongregation()
    {
        if( GameState.Instance.CanAffordDevotion( 250 ) )
        {
            GameState.Instance.ConsumeDevotion( 250 );

            GameObject congregation = Instantiate( CongregationPrefab );

            Vector3 position = transform.position;
            position.y += Random.Range( -2.0f, 2.0f );
            position.x += Random.value > 0.5f ? 2.0f : -2.0f;
            congregation.transform.position = position;
            congregation.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;

            congregation.GetComponent<Congregation>().NumFollowers = 50;
        }
    }

    public bool GetIfRequireUIUpdate()
    {
        bool hasOccupants = GetComponent<Location>().occupants != null;
        bool result = false;

        if( hasOccupants != m_HadOccupantsLastFrame )
        {
            result = true;            
        }

        m_HadOccupantsLastFrame = hasOccupants;

        return result;
    }

    public void ChannelDoomPortal()
    {
        if( GetComponent<Location>().occupants != null )
            m_Channeling = !m_Channeling;
    }

    public void CongregationLeave()
    {
        m_Channeling = false;
        GetComponent<Location>().occupants.Leave();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickPlayRandomClip : MonoBehaviour
{

    public List<AudioClip> clips;
    public float minPitch;
    public float maxPitch;
    public int LastIndex = 0;
    public bool RepeatAfterDone = false;
    public float pausebetweensounds = 5f;
    private float pausetimer = 0f;

    public void Start()
    {
        PlaySound();
    }

    public void Update()
    {

        if( RepeatAfterDone )
        {
            if( pausebetweensounds > 0f )
            {
                if( pausetimer > Time.time )
                {
                    return;
                }
            }
            if( !GetComponent<AudioSource>().isPlaying )
            {               
                PlaySound();                
            }
            else
            {
                pausetimer = pausebetweensounds + Time.time;
            }
        }
    }

    private void PlaySound()
    {

        int index = Random.Range( 0, clips.Count - 1 );
        if( index == LastIndex )
        {
            index = Random.Range( 0, clips.Count - 1 );
        }
        GetComponent<AudioSource>().pitch = Random.Range( minPitch, maxPitch );
        GetComponent<AudioSource>().PlayOneShot( clips[index] );
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Message : MonoBehaviour
{
    public static Message Instance { get; private set; }

    public TextMeshProUGUI Text;

    private float m_Timeout;

    void Start()
    {
        Instance = this;
    }

    public void SetText( string t, float duration )
    {
        Text.text = t;
        m_Timeout = duration;
    }

    public void FixedUpdate()
    {
        m_Timeout -= Time.deltaTime;

        Text.enabled = m_Timeout > 0.0f;
    }
}

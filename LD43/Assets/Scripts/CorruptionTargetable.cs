using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CorruptionTargetable : MonoBehaviour, IPointerClickHandler
{
    private float m_CorruptionTimer;
    private List<GameObject> m_LingeringInstances = new List<GameObject>();
    private float m_InquisitionKillTimer;
    private float m_CongregationKillTimer;
    
    public GameObject Indicator;
    public List<GameObject> OnCastFX;    
    public List<GameObject> LingeringFX;
    
    public bool IsCorrupted { get { return m_CorruptionTimer > 0.0f; } }

    public void OnPointerClick( PointerEventData eventData )
    {
        GameState gameState = GameState.Instance;
        if( gameState.CastingSpell == SpellType.Corrupt )
        {
            if( !gameState.GetIfInfluence( transform.position ) )
            {
                Message.Instance.SetText( "Outside of influence range.", 5.0f );
            }
            else 
            {
                if( gameState.CanAffordDevotion( 200 ) )
                {
                    m_CorruptionTimer = 25.0f;
                    gameState.ConsumeDevotion( 200 );
                    gameState.SetCastingSpell( SpellType.None );

                    foreach( GameObject fx in OnCastFX )
                    {
                        GameObject fxInstance = Instantiate( fx );
                        fxInstance.transform.position = transform.position + new Vector3( Random.Range( -4.0f, 4.0f ), 0.0f, Random.Range( -4.0f, 4.0f ) );
                   
                    }

                    foreach( GameObject fx in LingeringFX )
                    {
                        GameObject fxInstance = Instantiate( fx );
                        fxInstance.transform.position = transform.position;

                        m_LingeringInstances.Add( fxInstance );
                    }
                }
                else
                {
                    Message.Instance.SetText( "Not enough devotion.", 5.0f );
                }
            }
        }
    }

    public void FixedUpdate()
    {
        if( IsCorrupted )
            Indicator.SetActive( false );
        else 
            Indicator.SetActive( GameState.Instance.CastingSpell == SpellType.Corrupt );

        if( m_CorruptionTimer > 0.0f )
        {
            m_CorruptionTimer -= Time.deltaTime;

            if( m_CorruptionTimer <= 0.0f )
            {
                foreach( GameObject fx in m_LingeringInstances )
                {
                    Destroy( fx );
                }
                m_LingeringInstances.Clear();
            }           

            Collider[] colliders = Physics.OverlapSphere( transform.position, 5.0f );
            foreach( Collider collider in colliders )
            {
                Inquisition inquisition = collider.GetComponent<Inquisition>();
                if( inquisition != null )
                {
                    m_InquisitionKillTimer += Time.deltaTime;
                    float killRate = 0.1f;

                    if( m_InquisitionKillTimer > killRate )
                    {
                        m_InquisitionKillTimer -= killRate;
                        inquisition.NumInquisitors--;
                        if( inquisition.NumInquisitors == 0 )
                            Destroy( inquisition.gameObject );
                    }
                }

                Congregation congregation = collider.GetComponent<Congregation>();
                if( congregation != null )
                {
                    m_CongregationKillTimer += Time.deltaTime;
                    float killRate = 4f;

                    if( m_CongregationKillTimer > killRate )
                    {
                        m_CongregationKillTimer -= killRate;
                        congregation.NumFollowers--;
                    }
                }
            }
        }
    }
}

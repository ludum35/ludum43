using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIActionPanel : MonoBehaviour
{
    public GameObject btnPrefab;
    public LayoutGroup layoutGroup;

    internal void Init( Congregation selected )
    {
        ClearChildren();
        for ( int i = 0; i < selected.unitActions.Count; i++ )
        {
            UnitActionCallBackPair action = selected.unitActions[i];

            bool interactable = true;
            if( action.id == ActionID.Recruit || action.id == ActionID.Sacrifice || action.id == ActionID.MakeStronghold || action.id == ActionID.LeaveTown )
            {
                interactable = selected.m_Occupying != null;
            }

            GameObject btn = Instantiate( btnPrefab, layoutGroup.transform );
            btn.GetComponent<UIUnitAction>().Init( action.callback, action.id, selected );
            btn.GetComponent<Button>().interactable = interactable;
        }
    }

    public void Init( Stronghold selected )
    {
        ClearChildren();
        for ( int i = 0; i < selected.actions.Count; ++i )
        {
            bool active = true;

            Congregation occupants = selected.GetComponent<Location>().occupants;

            bool hasOccupants = occupants != null;

            UnitActionCallBackPair action = selected.actions[i];

            if ( !hasOccupants && action.id == ActionID.DoomPortal )
                active = false;

            if ( !hasOccupants && action.id == ActionID.LeaveTown )
                active = false;

            GameObject btn = Instantiate( btnPrefab, layoutGroup.transform );

            btn.GetComponent<UIUnitAction>().Init( action.callback, action.id, occupants );
            btn.GetComponent<Button>().interactable = active;
        }
    }

    private void ClearChildren()
    {
        RectTransform[] children = layoutGroup.GetComponentsInChildren<RectTransform>();

        foreach ( RectTransform t in children )
        {
            if ( t != layoutGroup.GetComponent<RectTransform>() )
            {
                Destroy( t.gameObject );
            }
        }
        children = null;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface ITargetingUser
{
    GameObject gameObject { get; }

    Action<ITargetingUser> requestActive { get; set; }

    void setActive();

    void setInActive();
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent( typeof( NavMeshAgent ) )]
public class Inquisition : MonoBehaviour
{
    enum State
    {
        MoveToLocation,
        FightingInTown,

        // Waiting after arrival/fighting in town
        TimerStart,
        ClearWait,

        Chasing
    }


    public GameObject FightFX;
    private List<GameObject> onGoingFX = new List<GameObject>();
    private float m_FXcoolDown;
    private float m_FXinterval = 1f;
    
    private State m_State;
    private Location m_TargetLocation;
    private bool m_StartedPath;
    private Congregation m_TargetCongregation;
    private float m_RadarTimer;
    private float m_GuardTimer;
    private NavMeshAgent m_NavMeshAgent;
    private float m_FightTimer;

    public static float Efficiency { get; set; }

    public int NumInquisitors { get; set; }

    void Start()
    {
        GetComponent<Party>().getNumFollowers = () => NumInquisitors;

        m_State = State.MoveToLocation;
        m_StartedPath = false;

        if( NumInquisitors == 0 )
            NumInquisitors = 10;

        m_NavMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void OnDestroy()
    {
        InquisitorOrigin.ArrivingArmies.Remove( m_TargetLocation );
    }

    private void OnGUI()
    {
        //if( GetComponent<FogOfWarHider>().IsVisible )
        //{
        //    Vector3 screenPos = Camera.main.WorldToScreenPoint( transform.position );
        //    screenPos.y = Screen.height - screenPos.y;
        //    GUI.Label( new Rect( screenPos, Vector3.one * 100.0f ), "" + NumInquisitors );
        //}
    }


    void FixedUpdate()
    {
        m_FXcoolDown += Time.deltaTime;

        GameState gameState = GameState.Instance;

        GetComponent<Party>().SetNumFollowers( NumInquisitors );

        m_NavMeshAgent.speed = ( 2.6f + Efficiency * 4.0f ) * Settings.GlobalMSScale;

        if( gameState.IsSlowActive )
        {
            if( GetComponent<FogOfWarHider>().IsVisible )
                m_NavMeshAgent.speed *= 0.25f;
        }

        switch( m_State )
        {
            case State.MoveToLocation:
                if( m_TargetLocation != null )
                {
                    if( !m_StartedPath )
                    {
                        NavMeshAgent agent = GetComponent<NavMeshAgent>();
                        agent.SetDestination( m_TargetLocation.transform.position );
                        m_StartedPath = true;
                    }
                    else
                    {
                        if( Vector3.Distance( m_TargetLocation.transform.position, transform.position ) < 1.0f )
                            m_State = State.FightingInTown;
                    }
                }
                break;

            case State.FightingInTown:
                Congregation target = null;

                if( m_TargetLocation != null )
                {
                    Location location = m_TargetLocation.GetComponent<Location>();

                    if( location != null )
                    {
                        target = location.occupants;
                    }
                }


                if( target != null )
                {
                    FightCongregation( target );
                }
                else
                {
                    m_State = State.TimerStart;
                }
                break;

            case State.TimerStart:
                m_RadarTimer = 0.0f;
                m_GuardTimer = 0.0f;
                m_State = State.ClearWait;
                break;

            case State.ClearWait:
                m_RadarTimer += Time.deltaTime;
                m_GuardTimer += Time.deltaTime;

                if( m_RadarTimer >= 1.0f )
                {

                    Collider[] colliders = Physics.OverlapSphere( transform.position, 7.5f );
                    foreach( Collider collider in colliders )
                    {
                        if( collider.GetComponent<Congregation>() )
                        {
                            m_TargetCongregation = collider.GetComponent<Congregation>();
                            m_State = State.Chasing;
                        }
                    }
                    m_RadarTimer -= 1.0f;
                }

                if( m_GuardTimer >= 60.0f )
                {
                    Destroy( gameObject );
                }
                break;

            case State.Chasing:
                if( m_TargetCongregation == null )
                {
                    MoveToLocation( m_TargetLocation );
                    break;
                }

                m_NavMeshAgent.SetDestination( m_TargetCongregation.transform.position );

                float d = Vector3.Distance( m_TargetCongregation.transform.position, transform.position );
                if( d > 10.0f )
                {
                    MoveToLocation( m_TargetLocation );
                    
                    break;
                }
                else if( d < 1.0f )
                {
                    FightCongregation( m_TargetCongregation );
                    m_NavMeshAgent.SetDestination( transform.position );
                }
                break;

        }
    }

    public void MoveToLocation( Location location )
    {
        m_StartedPath = false;
        m_State = State.MoveToLocation;
        m_TargetLocation = location;
    }

    public void FightCongregation( Congregation congregation )
    {
        // balance or something
        float killChance = 0.5f + Mathf.Lerp( 0.0f, 0.5f, Efficiency );

        float nextAttack = 5.0f / NumInquisitors;

        m_FightTimer += Time.deltaTime;
        

        while( m_FightTimer > nextAttack )
        {
            m_FightTimer -= nextAttack;

            if( m_FXcoolDown > m_FXinterval )
            {
                DoFightFX();
                m_FXcoolDown = 0f;
            }

            if( Random.value > 0.5f )
                congregation.NumFollowers--;
        }

    }

    public void DoFightFX()
    {
        for( int i = 0; i < 5; i++ )
        {
            if( onGoingFX.Count <= i )
            {
                onGoingFX.Add( null );
            }
            if ( onGoingFX[i] == null )
            {
                GameObject fx = Instantiate( FightFX );
                fx.transform.position = transform.position;

                onGoingFX[i] = fx;
                break;
            }
        }
    }
}

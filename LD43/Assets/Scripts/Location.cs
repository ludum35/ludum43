using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Location : MonoBehaviour, IPointerClickHandler
{
    public Congregation occupants;

    public void OnPointerClick( PointerEventData eventData )
    {
        if( eventData.button == PointerEventData.InputButton.Right )
        {
            Congregation congregation = SelectionManager.Instance.SelectedCongregation;

            if( congregation != null )
            {
                congregation.SetDestination( this );
            }
        }
        else if( eventData.button == PointerEventData.InputButton.Left )
        {
            SelectionManager.Instance.Select( this );
        }
    }
    public bool Enter( Congregation congregation )
    {
        if ( occupants != null )
        {
            return false;
        }


        if ( InEntryRange( congregation.transform ) )
        {
            occupants = congregation;
            return true;
        }

        return false;
    }

    public void Leave()
    {
        if ( occupants != null )
        {
            occupants = null;
        }
    }

    public bool InEntryRange( Transform other )
    {
        return ( Vector3.Magnitude( transform.position - other.position ) < GameState.Instance.UnitEntryDistance );
    }
}

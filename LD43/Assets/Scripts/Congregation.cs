using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.AI;
using System;

public struct UnitActionCallBackPair
{
    public ActionID id { get; private set; }
    public Action callback { get; private set; }

    public UnitActionCallBackPair( ActionID id, Action callback )
    {
        this.id = id;
        this.callback = callback;
    }
}


[RequireComponent( typeof( NavMeshAgent ) )]
public class Congregation : MonoBehaviour, IPointerClickHandler
{
    private Location m_DestinationLocation;

    public Location m_Occupying { get; private set; }

    [SerializeField]
    private GameObject visuals;

    public GameObject StrongholdPrefab;

    public GameObject activeFX;

    public float actionTimer { get; private set; }
    public float actionInterval { get; private set; }
    public float normalizedActionTime { get { return actionTimer / actionInterval; } }
    public ActionID activeAction { get; private set; }

    private Coroutine timerCoroutine = null;
    private Coroutine waitForTargetCoroutine = null;
    private Coroutine TravelAndEnterCoroutine = null;

    public GameObject SpellSlowFX;
    public GameObject SpellSpeedFX;
    private GameObject ActiveSlowFX;
    private GameObject ActiveSpeedFX;

    private NavMeshAgent m_NavMeshAgent;

    public int NumFollowers { get; set; }
    

    public Location Location { get { return m_Occupying; } }

    public List<UnitActionCallBackPair> unitActions = new List<UnitActionCallBackPair>();

    public static int NumCongregations;

    private void Start()
    {
        GetComponent<Party>().getNumFollowers = () => NumFollowers;

        actionTimer = 0f;
        actionInterval = 0f;

        if( NumFollowers == 0 )
            NumFollowers = 50;

        unitActions.AddRange( new UnitActionCallBackPair[] {

        new UnitActionCallBackPair( ActionID.EnterTown, () => StartEnter() ),
        new UnitActionCallBackPair( ActionID.LeaveTown, () => Leave() ),
        new UnitActionCallBackPair( ActionID.Recruit, () => StartRecruit() ),
        new UnitActionCallBackPair( ActionID.Sacrifice, () => StartSacrifice() ),
        new UnitActionCallBackPair( ActionID.MakeStronghold, () => MakeStrongHold() )

        } );


        m_NavMeshAgent = GetComponent<NavMeshAgent>();

        NumCongregations++;
    }

    private void OnDestroy()
    {
        NumCongregations--;
    }


    public void Update()
    {
        GetActionFX();
        GetSpellFX();
    }


    public void GetSpellFX()
    {
        if( GameState.Instance.IsSlowActive )
        {
            if( ActiveSlowFX == null )
            {
                CreateSpell( SpellSlowFX, ref ActiveSlowFX );
            }
        }
        else
        {
            if( ActiveSlowFX != null )
            {
                Destroy( ActiveSlowFX );
            }
        }
        if( GameState.Instance.IsSpeedActive )
        {
            if( ActiveSpeedFX == null )
            {
                CreateSpell( SpellSpeedFX, ref ActiveSpeedFX );
            }
        }
        else
        {
            if( ActiveSpeedFX != null )
            {
                Destroy( ActiveSpeedFX );
            }
        }
    }

    private void CreateSpell( GameObject prefab, ref GameObject activeFX )
    {
        if( prefab == null )
        {
            return;
        }
        activeFX = Instantiate( prefab );
        activeFX.transform.position = transform.position;
        activeFX.transform.SetParent( transform, true );
    }

    private void GetActionFX()
    {
        if( m_Occupying != null )
        {
            Stronghold t = m_Occupying.GetComponent<Stronghold>();
            if( t != null )
            {
                if( t.m_Channeling == true )
                {
                    if( GameState.Instance.CanAffordDevotion( 1 ) )
                    {
                        activeAction = ActionID.DoomPortal;
                    }
                    else
                    {
                        activeAction = ActionID.None;
                    }
                }
            }
        }
        if( activeAction != ActionID.None )
        {
            UnitAction t = UnitActionDB.Instance.getAction( activeAction );
            if( activeFX == null )
            {

                if( t.FX != null )
                {
                    activeFX = Instantiate( t.FX );
                    activeFX.transform.position = transform.position;
                    activeFX.name = t.FX.name;
                }
            }
            else if( t.FX == null || ( activeFX.name != t.FX.name ) )
            {
                Destroy( activeFX );
            }
        }
        else
        {
            if( activeFX != null )
            {
                Destroy( activeFX );
            }
        }

    }


    public void FixedUpdate()
    {
        if( NumFollowers <= 0 )
        {
            Destroy( gameObject );
            if( m_Occupying != null )
                m_Occupying.occupants = null;
        }

        m_NavMeshAgent.speed = ( 5.0f - Mathf.Lerp( 0.0f, 3.0f, ( NumFollowers - 100.0f ) / 900.0f ) ) * Settings.GlobalMSScale;

        GetComponent<Party>().SetNumFollowers( NumFollowers );
    }


    public void OnPointerClick( PointerEventData eventData )
    {
        if( eventData.button == PointerEventData.InputButton.Left )
            SelectionManager.Instance.Select( this );
    }

    private void OnGUI()
    {
        //Vector3 screenPos = Camera.main.WorldToScreenPoint( transform.position );
        //screenPos.y = Screen.height - screenPos.y;
        //GUI.Label( new Rect( screenPos, Vector3.one * 100.0f ), "" + NumFollowers );
    }

    bool wasInLocationLastTime = false;
    public bool GetIfRequireUIUpdate()
    {
        bool inLocation = m_Occupying != null;
        bool result = wasInLocationLastTime != inLocation;
        wasInLocationLastTime = inLocation;
        return result;
    }


    public void SetDestination( Vector3 target )
    {
        m_NavMeshAgent.SetDestination( target );

        m_DestinationLocation = null;
    }

    public void SetDestination( Location target )
    {
        SetDestination( target.transform.position );

        m_DestinationLocation = target;

        if( TravelAndEnterCoroutine != null )
        {
            StopCoroutine( TravelAndEnterCoroutine );
        }

        TravelAndEnterCoroutine = StartCoroutine( TravelAndEnter( target ) );
    }

    private IEnumerator TravelAndEnter( Location target )
    {
        while( true )
        {
            if( m_DestinationLocation != target )
            {
                yield break;
            }

            if( target.InEntryRange( this.transform ) )
            {
                Enter( target );
                yield break;
            }

            yield return new WaitForEndOfFrame();
        }
    }

    public void HideOnMap()
    {
        visuals.SetActive( false );
        m_NavMeshAgent.isStopped = true;
    }

    public void ShowOnMap()
    {
        visuals.SetActive( true );
        m_NavMeshAgent.isStopped = false;
    }

    public void StartEnter()
    {
        StopAction();
        activeAction = ActionID.EnterTown;
        waitForTargetCoroutine = StartCoroutine( SelectionManager.Instance.getTarget(
        ( x ) =>
        {
            if( x.GetComponent<Location>() )
            {
                Enter( x.GetComponent<Location>() );
            }
        } ) );
    }

    private void Enter( Location t )
    {
        if( !t.InEntryRange( this.transform ) )
        {
            SetDestination( t.GetComponent<Location>() );
        }
        else
        {
            if( t.Enter( this ) )
            {
                m_Occupying = t;
                HideOnMap();

                if( t.GetComponent<Town>() != null )
                {
                    InquisitorOrigin origin = InquisitorOrigin.GetClosest( t.transform.position );
                    if( origin != null )
                        origin.SpawnInquisition( t );
                }
            }
        }
    }

    public void StartSacrifice()
    {
        StopAction();

        if( m_Occupying == null )
        {
            return;
        }
        activeAction = ActionID.Sacrifice;
        timerCoroutine = StartCoroutine( CallbackTimer( GameState.Instance.SacrificeInterval, () => Sacrifice() ) );
    }

    private void StopAction()
    {
        activeAction = ActionID.None;
        if( timerCoroutine != null )
        {
            StopCoroutine( timerCoroutine );
        }
        if( waitForTargetCoroutine != null )
        {
            StopCoroutine( waitForTargetCoroutine );
        }
    }

    private void Sacrifice()
    {
        int SacrificialAmount = NumFollowers / 5;
        //GameState.Instance.AddDevotion( NumFollowers );
        GameState.Instance.AddDevotion(SacrificialAmount*3);
        NumFollowers -= (SacrificialAmount);

        
    }

    public void MakeStrongHold()
    {
        StopAction();

        if( m_Occupying == null )
        {
            return;
        }
        activeAction = ActionID.MakeStronghold;
        float time = (float)GameState.Instance.StrongholdInterval / (float)NumFollowers;

        timerCoroutine = StartCoroutine( CallbackTimer( time, () => ConvertToStronghold() ) );
    }

    public void StartRecruit()
    {
        StopAction();

        if( m_Occupying == null )
        {
            return;
        }
        activeAction = ActionID.Recruit;
        timerCoroutine = StartCoroutine( CallbackTimer( GameState.Instance.RecruitInterval, () => Recruit() ) );
    }

    private void ConvertToStronghold()
    {
        Vector3 position = m_Occupying.transform.position;
        Location old = m_Occupying;
        Leave();

        old.gameObject.GetComponent<Party>().SetNumFollowers( 0 );

        Destroy( old.gameObject );

        GameObject stronghold = Instantiate( StrongholdPrefab );
        stronghold.transform.position = position;

        StopAction();

    }

    private void Recruit()
    {
        if( m_Occupying == null )
        {
            StopAction();
            return;
        }

        int nrGained = 0;
        int nrOfRecruitments = 0;
        int i = 0;
        for( ; i < NumFollowers; i++ )
        {
            if( i >= m_Occupying.GetComponent<Town>().NumVillagers )
            {
                break;
            }

            float random = UnityEngine.Random.Range( 0f, 1f );
            if( random <= GameState.Instance.RecruitmentInitiatedChance )
            {
                nrOfRecruitments++;
                random = UnityEngine.Random.Range( 0f, 1f );
                if( random >= GameState.Instance.RecruitmentSuccessChance )
                {
                    nrGained++;
                }
            }
        }
        m_Occupying.GetComponent<Town>().KillVillagers( nrOfRecruitments );
        GameEvents.Instance.CreateWorldUI( GameEvents.Type.Positive, string.Format( "+{0}", nrGained ), transform.position );

        NumFollowers += nrGained;
    }

    internal void Leave()
    {
        StopAction();
        if( m_Occupying != null )
        {
            ShowOnMap();
            m_Occupying.Leave();
            m_Occupying = null;
        }
    }

    public IEnumerator CallbackTimer( float time, Action callback )
    {
        actionInterval = time;
        actionTimer = time;

        while( true )
        {
            yield return new WaitForEndOfFrame();


            if( GameState.Instance.IsSpeedActive )
                actionTimer -= Time.deltaTime * 1.7f;
            else
                actionTimer -= Time.deltaTime;

            if( actionTimer < 0f )
            {
                if( callback != null )
                {
                    callback();
                }

                actionTimer = time;
            }
        }
    }


}

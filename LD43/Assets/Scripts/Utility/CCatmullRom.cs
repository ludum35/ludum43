﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCatmullRom
{
    public Vector2 p0 { get; private set; }
    public Vector2 p1 { get; private set; }
    public Vector2 p2 { get; private set; }
    public Vector2 p3 { get; private set; }

    public float Length { get { return t2 - t1; } }

    private float t0;
    private float t1;
    private float t2;
    private float t3;

    private float alpha = 0.5f;

    public CCatmullRom( Vector2 a, Vector2 b, Vector2 c, Vector2 d )
    {
        p0 = a;
        p1 = b;
        p2 = c;
        p3 = d;

        t0 = 0.0f;
        t1 = GetT( t0, p0, p1 );
        t2 = GetT( t1, p1, p2 );
        t3 = GetT( t2, p2, p3 );
    }

    public Vector2 GetPointDenormalized( float t )
    {
        t = t1 + t;

        Vector2 A1 = ( t1 - t ) / ( t1 - t0 ) * p0 + ( t - t0 ) / ( t1 - t0 ) * p1;
        Vector2 A2 = ( t2 - t ) / ( t2 - t1 ) * p1 + ( t - t1 ) / ( t2 - t1 ) * p2;
        Vector2 A3 = ( t3 - t ) / ( t3 - t2 ) * p2 + ( t - t2 ) / ( t3 - t2 ) * p3;

        Vector2 B1 = ( t2 - t ) / ( t2 - t0 ) * A1 + ( t - t0 ) / ( t2 - t0 ) * A2;
        Vector2 B2 = ( t3 - t ) / ( t3 - t1 ) * A2 + ( t - t1 ) / ( t3 - t1 ) * A3;

        Vector2 C = ( t2 - t ) / ( t2 - t1 ) * B1 + ( t - t1 ) / ( t2 - t1 ) * B2;

        return C;
    }


    public Vector2 GetPoint( float t )
    {
        return GetPointDenormalized( t * Length );
    }

    float GetT( float t, Vector2 p0, Vector2 p1 )
    {
        float a = Mathf.Pow( ( p1.x - p0.x ), 2.0f ) + Mathf.Pow( ( p1.y - p0.y ), 2.0f );
        float b = Mathf.Pow( a, 0.5f );
        float c = Mathf.Pow( b, alpha );

        return ( c + t );
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class RectangleOutlineIterator : IEnumerable
{
    protected Vector2i min;
    protected Vector2i max;

    public RectangleOutlineIterator( Vector2i min, Vector2i max )
    {
        this.min = min;
        this.max = max;
    }

    public IEnumerator GetEnumerator()
    {
        for( int x = min.x; x <= max.x; ++x )
        {
            yield return new Vector2i( x, min.y );
            yield return new Vector2i( x, max.y );
        }

        for( int y = min.y + 1; y < max.y; ++y )
        {
            yield return new Vector2i( min.x, y );
            yield return new Vector2i( max.x, y );
        }

        yield break;
    }
}

public class RectangleOutlineNoCornersIterator : IEnumerable
{
    protected Vector2i min;
    protected Vector2i max;

    public RectangleOutlineNoCornersIterator( Vector2i min, Vector2i max )
    {
        this.min = min;
        this.max = max;
    }

    public IEnumerator GetEnumerator()
    {
        for( int x = min.x + 1; x < max.x - 1; ++x )
        {
            yield return new Vector2i( x, min.y );
            yield return new Vector2i( x, max.y - 1 );
        }

        for( int y = min.y + 1; y < max.y - 1; ++y )
        {
            yield return new Vector2i( min.x, y );
            yield return new Vector2i( max.x - 1, y );
        }

        yield break;
    }
}

public class RectangleFillIterator : IEnumerable
{
    protected Vector2i min;
    protected Vector2i max;

    public RectangleFillIterator( Vector2i min, Vector2i max )
    {
        this.min = min;
        this.max = max;
    }

    public IEnumerator GetEnumerator()
    {
        for( int y = min.y; y < max.y; ++y )
        {
            for( int x = min.x; x < max.x; ++x )
            {
                yield return new Vector2i( x, y );
            }
        }

        yield break;
    }
}

public class CircleOutlineIterator : IEnumerable
{
    protected int radius;
    protected Vector2i center;

    public CircleOutlineIterator( Vector2i center, int radius )
    {
        this.center = center;
        this.radius = radius;
    }


    public IEnumerator GetEnumerator()
    {
        for( float ly = radius; ly >= -radius; --ly )
        {
            int wy = (int)ly + center.y;

            float f = ly / (float)( radius );

            float a = Mathf.Acos( f );

            int xLim = (int)Mathf.RoundToInt( Mathf.Sin( a ) * (float)radius );

            if( xLim == 1 )
            {
                yield return new Vector2i( center.x, wy );
            }
            else if( xLim > 1 )
            {
                yield return new Vector2i( center.x + ( -xLim + 1 ), wy );
                yield return new Vector2i( center.x + ( xLim - 1 ), wy );
            }

            for( int lx = -xLim + 1; lx <= xLim - 1; ++lx )
            {
                int wx = center.x + lx;

                yield return new Vector2i( wx, wy );
            }
        }

        yield break;
    }
}

public class CircleFillIterator : IEnumerable
{
    protected Vector2i center;
    protected int radius;

    public CircleFillIterator( Vector2i center, int radius )
    {
        this.center = center;
        this.radius = radius;

    }


    public IEnumerator GetEnumerator()
    {
        for( float ly = radius; ly >= -radius; --ly )
        {
            int wy = (int)ly + center.y;

            float f = ly / (float)( radius );

            float a = Mathf.Acos( f );

            int xLim = (int)Mathf.RoundToInt( Mathf.Sin( a ) * (float)radius );

            for( int lx = -xLim + 1; lx <= xLim - 1; ++lx )
            {
                int wx = center.x + lx;

                yield return new Vector2i( wx, wy );
            }
        }

        yield break;
    }
}

public class FloodFillIterator2D : IEnumerable
{
    public delegate bool IsValid( Vector2i index );

    public IsValid ValidDelegate { get; private set; }
    public HashSet<Vector2i> Closed { get; private set; }

    protected HashSet<Vector2i> open;
    protected HashSet<Vector2i> newOpen;

    public FloodFillIterator2D( Vector2i first, IsValid validDelegate )
    {
        this.ValidDelegate = validDelegate;

        open = new HashSet<Vector2i>();
        Closed = new HashSet<Vector2i>();
        newOpen = new HashSet<Vector2i>();

        open.Add( first );
        Closed.Add( first );
    }

    public IEnumerator GetEnumerator()
    {
        while( open.Count > 0 )
        {
            foreach( Vector2i current in open )
            {
                yield return current;

                foreach( Vector2i offset in Vector2i.Neighbors )
                {
                    Vector2i neighbor = current + offset;
                    if( !Closed.Contains( neighbor ) && ValidDelegate( neighbor ) )
                    {
                        newOpen.Add( neighbor );
                        Closed.Add( neighbor );
                    }
                }
            }

            open = newOpen;
            newOpen = new HashSet<Vector2i>();
        }
    }
}
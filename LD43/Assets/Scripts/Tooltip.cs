﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Tooltip : MonoBehaviour
{
    public float Delay = 0.5f;

    private static int ID;

    public static int NextID { get { return ID++; } }

    public static Tooltip Instance { get; private set; }

    private int m_CurrentID;
    private float m_ShowTimer;

    private void Start()
    {
        Instance = this;

        GetComponent<CanvasGroup>().alpha = 0.0f;
        m_CurrentID = -1;
    }


    public void ShowTooltip( int id, Vector3 screenSpaceAnchor, string text )
    {
        if( id != m_CurrentID )
        {
            transform.position = screenSpaceAnchor;
            GetComponent<CanvasGroup>().alpha = 0.0f;
            m_ShowTimer = Delay;
            GetComponentInChildren<TextMeshProUGUI>().text = text;
            m_CurrentID = id;
        }
    }

    public void EndTooltip( int id )
    {
        if( id == m_CurrentID )
        {
            GetComponent<CanvasGroup>().alpha = 0.0f;
            m_CurrentID = -1;
        }
    }

    private void FixedUpdate()
    {
        if( m_CurrentID >= 0 )
        {
            m_ShowTimer -= Time.deltaTime;

            if( m_ShowTimer <= 0.0f )
            {
                GetComponent<CanvasGroup>().alpha += 10.0f * Time.deltaTime;
            }
        }
    }
}

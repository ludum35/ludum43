using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MoveSoundsFX : MonoBehaviour
{

    public List<AudioClip> clips;
    public Vector3 lastPos;
    public float walkCD = .2f;
    private float lastWalk = 0f;
    // Update is called once per frame
    void Update()
    {

        if( ( transform.position - lastPos ).magnitude > .1f )
        {
            if( walkCD + lastWalk > Time.time )
            {
                return;
            }
            else
            {
                lastWalk = Time.time;
            }
            int playindex = Random.Range( 0, clips.Count );
            GetComponent<AudioSource>().PlayOneShot( clips[playindex] );
            lastPos = transform.position;
        }
    }
}

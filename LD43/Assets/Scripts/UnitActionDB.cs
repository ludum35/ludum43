using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class UnitActionDB : MonoBehaviour
{
    public static UnitActionDB Instance { get; private set; }

    public List<UnitAction> unitActions;

    public UnitAction getAction( ActionID id )
    {
        return unitActions.Find( ( x ) => x.ID == id );
    }

    private void Start()
    {
        Instance = this;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevotionScaler : MonoBehaviour
{
    private Vector3 m_StartScale;

    void Start()
    {
        m_StartScale = transform.localScale;
    }

    void FixedUpdate()
    {
        transform.localScale = Mathf.Lerp( 1.0f, 5.0f, GameState.Instance.DevotionScalar ) * m_StartScale;
    }
}

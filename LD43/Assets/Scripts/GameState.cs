using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpellType
{
    None,
    Corrupt,
    Speed,
    Slow
}

// fuk u
public enum GameStateState
{
    Game,
    Defeat,
    Victory
}

[DefaultExecutionOrder( -50 )]
public class GameState : MonoBehaviour, ITargetingUser
{
    public static GameState Instance { get; private set; }

    const float MaxDevotion = 1500.0f;

    public const float MaxChannelProgress = 1500.0f;

    public float Devotion { get; private set; }

    public float ChannelProgress { get; set; }

    public float DevotionScalar { get; private set; }

    public SpellType CastingSpell { get; private set; }

    public bool IsSlowActive { get { return m_SlowDuration >= 0.0f; } }

    public bool IsSpeedActive { get { return m_SpeedDuration >= 0.0f; } }

    public Action<ITargetingUser> requestActive { get; set; }
    public bool isActiveTargeting = false;

    public GameObject FXSpellSlow;
    public GameObject FXSpellSpeed;

    private float m_DisplayedDevotion;

    public RenderTexture FogTexture;

    [Header( "Recruitment" )]
    [Range( 0, 1 ), Tooltip( "Chance that a cultist will take a villager for recruitment." )]
    public float RecruitmentInitiatedChance;

    [Range( 0, 1 ), Tooltip( "Chance that a recruited villager gets turned to a cultist, otherwise dies." )]
    public float RecruitmentSuccessChance;


    [Header( "Action timers" )]
    public float SacrificeInterval = 10f;
    public float RecruitInterval = 10f;
    public float StrongholdInterval = 10f;
    public float UnitEntryDistance = 1.5f;

    private float m_SlowDuration;
    private float m_SpeedDuration;

    private GameStateState m_State;

    // Use this for initialization
    void Start()
    {
        Instance = this;

        Devotion = 100;

        ActiveTargetingManager.Instance.Register( this, false );

        m_State = GameStateState.Game;
    }

    // Update is called once per frame
    void Update()
    {
        if( m_State == GameStateState.Game )
        {
            m_DisplayedDevotion = Mathf.MoveTowards( m_DisplayedDevotion, Devotion, 500.0f * Time.deltaTime );

            GameUI.Instance.Devotion.text = string.Format( "Devotion: {0}/1500", (int)m_DisplayedDevotion );
            GameUI.Instance.ChannelProgress.text = string.Format( "Doom Portal Progress: {0}%", ( ChannelProgress / MaxChannelProgress * 100.0f ).ToString( "0.0" ) );

            float targetDevotionScalar = Devotion / MaxDevotion;
            DevotionScalar = Mathf.MoveTowards( DevotionScalar, targetDevotionScalar, 0.1f * Time.deltaTime );

            
            if( CastingSpell != SpellType.None && Input.GetMouseButtonDown( 1 ) )
                SetCastingSpell( SpellType.None );

            if( CastingSpell == SpellType.Corrupt )
            {
                TryCastCorruption();
            }

            if( CastingSpell == SpellType.Slow )
                CastSlow();

            if( CastingSpell == SpellType.Speed )
                CastSpeed();

            Inquisition.Efficiency = Mathf.Clamp01( Time.time / ( 60 * 30 ) );
            m_SlowDuration -= Time.deltaTime;
            m_SpeedDuration -= Time.deltaTime;

            if( Congregation.NumCongregations == 0 )
            {
                m_State = GameStateState.Defeat;
                GameUI.Instance.DefeatPanel.SetActive( true );
            }

            if( ChannelProgress >= MaxChannelProgress )
            {
                m_State = GameStateState.Victory;
                GameUI.Instance.VictoryPanel.SetActive( true );
            }
        }

    }

    public void TryCastCorruption()
    {
        if( !CanAffordDevotion( 200 ) )
        {
            Message.Instance.SetText( "Not enough devotion", 1f );
            Instance.SetCastingSpell( SpellType.None );
            return;
        }

    }

    public void CastSpeed()
    {
        if( CanAffordDevotion( 100 ) )
        {
            if( !IsSpeedActive )
            {
                m_SpeedDuration = 60.0f;
                ConsumeDevotion( 100 );
            }
        }
        else
            Message.Instance.SetText( "Not enough devotion.", 5.0f );

        SetCastingSpell( SpellType.None );
    }

    public void CastSlow()
    {
        if( CanAffordDevotion( 75 ) )
        {
            if( !IsSlowActive )
            {
                m_SlowDuration = 25.0f;
                ConsumeDevotion( 75 );
            }
        }
        else
            Message.Instance.SetText( "Not enough devotion.", 5.0f );

        SetCastingSpell( SpellType.None );
    }

    public bool CanAffordDevotion( int requiredAmount )
    {
        return Devotion >= requiredAmount;
    }

    public void ConsumeDevotion( int amount )
    {
        Devotion -= amount;
    }

    public void AddDevotion( int amount )
    {
        Devotion = Mathf.Min( Devotion + amount, MaxDevotion );
    }

    public bool GetIfInfluence( Vector3 worldPoint )
    {
        if( FogCamera.Instance != null )
        {
            float v = FogCamera.Instance.GetValue( worldPoint );

            return v > 0.75f;
        }
        return true;
    }

    public void SetCastingSpell( SpellType type )
    {
        CastingSpell = type;
        if( type == SpellType.None )
        {
            ActiveTargetingManager.Instance.GiveControl( this );
        }
        else
        {
            requestActive( this );
        }

        if( type == SpellType.Corrupt )
            Message.Instance.SetText( "Select corruption target", 5.0f );
    }

    public void setActive()
    {
        isActiveTargeting = true;
    }

    public void setInActive()
    {
        CastingSpell = SpellType.None;
        isActiveTargeting = false;
    }
}

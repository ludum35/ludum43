using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent( typeof( Location ) )]
public class Town : MonoBehaviour, IPointerClickHandler
{
    public int MaxVillagers = 500;

    public int NumVillagers { get; set; }

    private float m_GrowTimer;

    private float m_KillTimer;

    private float m_GrowthCooldown;

    void Start()
    {
        GetComponent<Party>().getNumFollowers = () => NumVillagers;
        NumVillagers = (int)UnityEngine.Random.Range( 100.0f, MaxVillagers );
        m_GrowTimer = UnityEngine.Random.Range( 0.0f, 1.0f );
    }

    void FixedUpdate()
    {
        GetComponent<Party>().SetNumFollowers( NumVillagers );

        CorruptionTargetable corruption = GetComponent<CorruptionTargetable>();

        if( corruption.IsCorrupted )
        {
            float delta = 1.0f / 25.0f;
            m_KillTimer += Time.deltaTime;

            while( m_KillTimer >= delta )
            {
                m_KillTimer -= delta;
                NumVillagers = Mathf.Max( 0, NumVillagers - 1 );
            }
        }
        else 
        {
            if( GetComponent<Location>().occupants == null )
            {
                m_GrowthCooldown -= Time.deltaTime;

                if( m_GrowthCooldown < 0.0f )
                {
                    m_GrowTimer += Time.deltaTime;
                    if( m_GrowTimer >= 1.0f )
                    {
                        NumVillagers = Mathf.Min( MaxVillagers,  NumVillagers + 1 );
                        m_GrowTimer -= 1.0f;
                    }
                }
            }
            else
            {
                m_GrowthCooldown = 30.0f;
            }
        }
    }

    private void OnGUI()
    {
        //Vector3 screenPos = Camera.main.WorldToScreenPoint( transform.position );
        //screenPos.y = Screen.height - screenPos.y;
        //GUI.Label( new Rect( screenPos, Vector3.one * 100.0f ), "" + NumVillagers );
    }

    public void OnPointerClick( PointerEventData eventData )
    {
        if ( eventData.button == PointerEventData.InputButton.Left )
            SelectionManager.Instance.Select( GetComponent<Location>() );


    }

    public void KillVillagers( int nr )
    {
        NumVillagers -= nr;
        GameEvents.Instance.CreateWorldUI( GameEvents.Type.Negative , string.Format( "-{0}", nr ), transform.position );
    }


}

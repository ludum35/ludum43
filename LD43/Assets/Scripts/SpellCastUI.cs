using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellCastUI : MonoBehaviour
{

    public void StartCast( int _type )
    {
        GameState.Instance.SetCastingSpell( (SpellType)_type );
    }
}

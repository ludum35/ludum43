﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InquisitorOrigin : MonoBehaviour
{
    public GameObject InquisitionPrefab;

    public static List<InquisitorOrigin> AllOrigins = new List<InquisitorOrigin>();

    public static Dictionary<Location, Inquisition> ArrivingArmies = new Dictionary<Location, Inquisition>();

    private void Start()
    {
        AllOrigins.Add( this );
    }

    public static InquisitorOrigin GetClosest( Vector3 position )
    {
        float bestDist = 28905623;
        InquisitorOrigin best = null;

        foreach( InquisitorOrigin origin in AllOrigins )
        {
            float d = Vector3.Distance( origin.transform.position, position );
            if( d < bestDist )
            {
                bestDist = d;
                best = origin;
            }
        }

        return best;
    }

    public void SpawnInquisition( Location target )
    {
        if( ArrivingArmies.ContainsKey( target ) )
            return;
        
        GameObject instance = Instantiate( InquisitionPrefab );
        instance.transform.position = transform.position;
        instance.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;

        instance.GetComponent<Inquisition>().MoveToLocation( target );


        int size = target.GetComponent<Town>().MaxVillagers / 10 + target.occupants.NumFollowers / 5;

        instance.GetComponent<Inquisition>().NumInquisitors = size;


        ArrivingArmies.Add( target, instance.GetComponent<Inquisition>() );
    }
}

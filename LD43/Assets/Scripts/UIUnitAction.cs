using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIUnitAction : MonoBehaviour
{
    public Image image;
    public Image bkFill;
    public Text textBox;

    [SerializeField]
    private Congregation Con;
    [SerializeField]
    private ActionID id;

    public void Init( Action onClick, ActionID id, Congregation con )
    {
        //TODO: add a tooltip when hovered over
        GetComponent<Button>().onClick.AddListener( () => onClick() );
        UnitAction t = UnitActionDB.Instance.getAction( id );
        Con = con;
        this.id = id;
        image.sprite = t.Icon;
        textBox.text = t.Name;

        GetComponent<HoverTooltip>().Text = t.ToolTip;
    }

    public void Update()
    {
        SetFill();

    }

    private void SetFill()
    {
        if( Con != null )
        {
            if( id == Con.activeAction )
            {
                bkFill.fillAmount = 1 - Con.normalizedActionTime;
                return;
            }
        }

        bkFill.fillAmount = 0f;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder( -100 )]
public class SelectionManager : MonoBehaviour, ITargetingUser
{
    public static SelectionManager Instance { get; private set; }

    public Location SelectedLocation { get; private set; }
    public Congregation SelectedCongregation { get; private set; }

    public Action<ITargetingUser> requestActive { get; set; }

    private bool isActive = false;
    public bool isTargeting = false;
    public GameObject lastTarget;

    private GameObject ActiveLocationUI;
    private GameObject ActiveCongregationUI;

    public GameObject LocationUI;
    public GameObject CongregationUI;


    private void Start()
    {
        Instance = this;

        ActiveTargetingManager.Instance.Register( this, true );
    }

    public void Update()
    {
        if( !isActive )
        {
            DeselectAll();
            DestroyWorldTracked();
            return;
        }
        if( SelectedLocation != null )
        {
            if( ActiveLocationUI == null )
            {
                ActiveLocationUI = Instantiate( LocationUI );
            }
            ActiveLocationUI.transform.position = SelectedLocation.transform.position;
        }
        else
        {
            Destroy( ActiveLocationUI );
        }
        if( SelectedCongregation != null )
        {
            if( ActiveCongregationUI == null )
            {
                ActiveCongregationUI = Instantiate( CongregationUI );
            }
            ActiveCongregationUI.transform.position = SelectedCongregation.transform.position;
        }
        else
        {
            Destroy( ActiveCongregationUI );
        }
    }

    public void DestroyWorldTracked()
    {
        if( ActiveLocationUI != null )
        {
            Destroy( ActiveLocationUI );
        }
        if( ActiveCongregationUI != null )
        {
            Destroy( ActiveCongregationUI );
        }
    }

    public void DeselectAll()
    {
        SelectedLocation = null;
        SelectedCongregation = null;
    }

    private bool Select( GameObject p )
    {
        if( isTargeting )
        {
            lastTarget = p;
            isTargeting = false;
            return false;
        }
        else
        {
            return true;
        }
    }

    public void Select( Location location )
    {
        if( !Select( location.gameObject ) )
        {
            return;
        }

        DeselectAll();

        SelectedLocation = location;
    }

    public void Select( Congregation congregation )
    {
        if( !Select( congregation.gameObject ) )
        {
            return;
        }

        DeselectAll();

        SelectedCongregation = congregation;
    }

    public IEnumerator getTarget( System.Action<GameObject> callback )
    {
        isTargeting = true;
        lastTarget = null;

        while( true )
        {
            if( !isTargeting )
            {
                callback( lastTarget );
                yield break;
            }

            yield return new WaitForEndOfFrame();
        }
    }

    public void setActive()
    {
        isActive = true;
    }

    public void setInActive()
    {
        isActive = false;

        DestroyWorldTracked();
        DeselectAll();
    }
}

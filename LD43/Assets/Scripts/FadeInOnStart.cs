﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInOnStart : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        GetComponent<CanvasGroup>().alpha = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<CanvasGroup>().alpha += 1.0f * Time.deltaTime;
    }
}

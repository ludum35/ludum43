using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleSpawnFX : MonoBehaviour
{

    public Material mat;
    public float fadeTime = 1f;
    public Color startColor;
    // Use this for initialization
    void Start()
    {
        startColor = mat.GetColor( "_Color" );
        startColor.a = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        startColor.a += ( ( 1f / fadeTime ) * Time.deltaTime );
        if( startColor.a > 1f )
        {
            startColor.a = 1f;
        }

        mat.SetColor( "_Color", startColor );
        if( startColor.a == 1f )
        {
            Destroy( this );
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkAllBridges : MonoBehaviour {

    public GameObject TargetingPrefab;
	// Use this for initialization
	void Start () {
        GameObject[] bridges = GameObject.FindGameObjectsWithTag( "Bridge" );

        foreach( GameObject p in bridges)
        {
            GameObject targeting = Instantiate( TargetingPrefab );
            targeting.transform.parent = p.transform;
            targeting.transform.localPosition = Vector3.zero;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

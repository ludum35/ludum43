using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FollowerCount : MonoBehaviour
{

    public TextMeshProUGUI textBox;
    public Party party;

    // Update is called once per frame
    void Update()
    {
        if( textBox != null && party != null )
        {
            textBox.text = string.Format( "{0}", party.getNumFollowers() );
        }
    }
}

﻿Shader "Unlit/FogOfWarLayerShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_CloudTex ("Texture", 2D) = "white" {}
		_FogColor ("Color", Color) = ( 0, 0, 0, 0.5 )
	}
	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent"}

		Cull Off 
		Lighting Off
		
		ZWrite Off
		ZTest Off

		Fog { Mode Off }

		Blend SrcAlpha OneMinusSrcAlpha 				

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			sampler2D _CloudTex;
			float4 _CloudTex_ST;

			float4 _FogColor;


			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv2 = TRANSFORM_TEX(v.uv, _CloudTex);


				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed col = tex2D(_MainTex, i.uv);
				fixed4 fog = tex2D(_CloudTex, i.uv2 + float2( _Time. x / 10.0f, 0.0f ) );	



				return fixed4( fog.rgb * _FogColor.rgb, min( 1 - col, _FogColor.a ) );
			}
			ENDCG
		}
	}
}
